SELECT film.id_genre, genre.name AS 'id_genre', film.id_distrib, distrib.name AS 'id_distrib', film.title AS 'title' 
FROM film
LEFT JOIN genre ON genre.id_genre=film.id_genre
LEFT JOIN distrib ON distrib.id_distrib=film.id_distrib
WHERE film.id_genre >= 4 AND film.id_genre <= 8;
