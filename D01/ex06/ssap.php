#!/usr/bin/php
<?php
function ft_split($s)
{
	$tab = explode(" ", $s);
	sort($tab);
	return($tab);
}
$i = 1;
$ret = array();
if ($argc < 2)
	exit();
while ($i < $argc)
{
	$tab = ft_split($argv[$i]);
	$ret = array_merge($ret, $tab);
	$i++;
}
sort($ret);
$i = 0;
$len = count($ret);
while ($i < $len)
{
	print($ret[$i]);
	print("\n");
	$i++;
}
?>
